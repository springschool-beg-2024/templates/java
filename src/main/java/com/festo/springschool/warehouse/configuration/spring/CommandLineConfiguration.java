package com.festo.springschool.warehouse.configuration.spring;

import com.beust.jcommander.JCommander;
import com.festo.springschool.warehouse.cli.CommandLineHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CommandLineConfiguration {

    @Bean
    public CommandLineHandler createCommandLineHandler(final JCommander jCommander) {
        final CommandLineHandler commandLineHandler = new CommandLineHandler();
        commandLineHandler.setJCommander(jCommander);
        return commandLineHandler;
    }

    @Bean
    public JCommander createJCommander() {
        return new JCommander();
    }

}
